Instructions to fix FFmpeg issue:

1. Download "ffmpeg-git-full.7z" from https://www.gyan.dev/ffmpeg/builds/
2. Extract the folder that is inside the 7z file
3. Rename such folder to "ffmpeg"
4. Put the folder into the root of the C drive
5. Add "C:\ffmpeg\bin" into the PATH env variable on Windows