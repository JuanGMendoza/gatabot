MESSAGES = [
['Holi @everyone ! Hoy es el cumple de ', ' . Pásenle un mensaje bonito y de paso le regalan unas manos a ver si comienza a jugar mejor :yellow_heart:'],

'El conductismo, según John B. Watson (uno de los primeros en definir el objeto de estudio de la psicología), es el estudio experimental objetivo y natural de la conducta.1​ Para Burrhus Frederic Skinner el conductismo es una filosofía de la ciencia de la conducta,2​ definió varios aspectos esenciales de su objeto de estudio y a diferencia de Watson se centró en describir las leyes generales que rigen la conducta.3​ El objeto de estudio de la psicología y la forma en cómo se concibe la conducta es entendida de diversos modos, según el enfoque desde el que se vea. Se pueden identificar más de diez formas de conductismo4​ , desde el propuesto por Watson hasta nuestros días; pasando por el conductismo de Tolman, Hull y Skinner, el interconductismo y la psicología interconductual de Kantor, el conductismo teleológico de Rachlin, empírico de Bijou, teórico de Staddon y biológico de Timberlake, el contextualismo funcional de Hayes, etc. Jacob Robert Kantor define el conductismo como «una renuncia a las doctrinas del alma, la mente y la conciencia», para ocuparse del «estudio de los organismos en interacción con sus ambientes». En términos más amplios, lo considera como equivalente al término ciencia (Kantor 1968, cit. por Campos, 1973, p. 91), dado que se ocupa de la naturaleza a partir del «principio del comportamiento». Así, la química estudia el comportamiento de los elementos y la sustancia, la física estudia el comportamiento de la materia y sus propiedades, la astronomía estudia el comportamiento de los astros y galaxias, y la psicología estudia las interacciones entre los organismos y su entorno.',

'Bro dame chance solo soy yo cocinando',

' acabas de conseguir la legendaria empanada con salsa! Pocas gatas tienen el privilegio de presenciar este evento milenario. Desde ahora, eres la gata elegida ¡Eres la gata ensalsada! @everyone',



]
