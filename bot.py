import asyncio
import discord
from discord.ext import commands, tasks
import asyncio
from random import randint
import tools
import datetime
import messages


intents = discord.Intents().all()
client = commands.Bot(command_prefix=".", intents=intents)

# Members of 4 bobos
bobos = ["kaehlus", "mornet", "malphac", "emmcarstairs"]
# Bots that we have in the server
bots = ["FredBoat♪♪", "GataBot"]

# The developer username
# Mornet#1054
devs = ["mornet", "kaehlus"]

gataChimba = []

# List tha will contain all the users that have used the empanada command within
# The last 2 seconds
empanadaTimer = []

# .otorrino command timeout
otorrinoTimeout = False

# bobos assemble timeout
assembleTimeout = False


@client.event
async def on_ready():

    # When the bot starts we need to read in the chimbas from our
    # text file database
    global gataChimba

    gataChimba = tools.update_database()
    print("ready")
    birthday.start()
    # We update our list every day
    while True:
        # await asyncio.sleep(86400)
        gataChimba = tools.update_database()


        await asyncio.sleep(86400)


@client.command(aliases=["Hola"])
async def hola(ctx):

    await ctx.send("Miau")


# Sends wikipedias explanation for conductismo
@client.command()
async def conductismo(ctx):

    await ctx.send(messages.MESSAGES[1])


# Sends a picture with the relationships between our discord members
@client.command()
async def relaciones(ctx):
    pic = discord.File("./media/relaciones.png")
    await ctx.send(file=pic)


# Sends a random picture of an empanada in the chat
# .1% probability of becoming la gata ensalsada
@client.command()
async def empanadas(ctx):

    global empanadaTimer
    # print('emp')
    # print("empanada" in ctx.channel.name)
    # We only allow this command in a specific channel
    if "empanada" in ctx.channel.name:

        # Make sure 2 seconds has passed since the last command
        if ctx.message.author in empanadaTimer:
            await ctx.send(messages.MESSAGES[2])

        else:
            empanadaTimer.append(ctx.message.author)

            rand = randint(0, 1000)

            # 2/1000 probability of getting the empanada with sauce
            if rand < 2:
                texto = messages.MESSAGES[3]
                ganador = ctx.message.author.name

                pic = discord.File("./media/empanadas/empanadaBendita.jpg")
                await ctx.send(file=pic)
                await ctx.send("¡Felicidades! " + ganador + texto)

                # we now retrieve all the roles in the server
                roles = await ctx.guild.fetch_roles()

                # Then we identify the gata ensalsada role
                for role in roles:
                    if "Ensalsada" in role.name:
                        gataEnsalsada = role

                # Exchange the roles
                viejaGata = gataEnsalsada.members[0]

                await viejaGata.remove_roles(gataEnsalsada)
                await ctx.message.author.add_roles(gataEnsalsada)

            # Normal empanadas
            else:

                randomPic = tools.random_file("./media/empanadas")

                while randomPic == "empanadaBendita.jpg":
                    randomPic = tools.random_file("./media/empanadas")

                pic = discord.File("./media/empanadas/" + randomPic)
                await ctx.send(file=pic)

            await asyncio.sleep(2)
            empanadaTimer.remove(ctx.message.author)

    else:
        await ctx.send("Para ver empanadas porfi vete a #empanada-spam")


# This command just plays an audio
# it has a 60 second anti-spam timeout
@client.command()
async def otorrino(ctx):

    global otorrinoTimeout

    if not otorrinoTimeout:
        otorrinoTimeout = True

        voiceChannel = ctx.message.author.voice.channel

        if voiceChannel != None:

            voice_client = await voiceChannel.connect()
            voice_client.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source="./media/otorrino.mp3"))
            await asyncio.sleep(3)
            await voice_client.disconnect()
            await asyncio.sleep(60)

        otorrinoTimeout = False
    else:
        await ctx.send("Naoko sigue en el otorrino")


# this command adds a username to the chimba database
# only Mornet should be able to do this
@client.command()
async def chimba(ctx):

    global gataChimba
    
    if str(ctx.message.author) in devs:

        chimba = ctx.message.content[8:]
        file = open("chimbas.txt", "a")
        today = datetime.date.today()
        formatDate = today.strftime("%m/%d/%y")
        file.write(chimba + " " + formatDate + "\n")

        file.close()
        await ctx.send("Estoy muy decepcionada de ti, " + chimba)

        gataChimba.append(chimba)

    else:
        await ctx.send("Quien te crees prro")


@client.command()
async def chimbas(ctx):

    chimbas = ""
    if len(gataChimba) == 0:
        await ctx.send("no hay gatas chimbas de momento :(")
    else:
        for gata in gataChimba:
            chimbas += "- " + gata + "\n"
        await ctx.send(chimbas)


# In this event my purpose was to run the avengers audio and the gata chimba audio
# whenever the appropriate people joined
@client.event
async def on_voice_state_update(member, before, after):

    global assembleTimeout

    if str(member.name) not in bots:
        # First we make sure someone joined or left the channel
        joined = True
        if after.channel != None:
            onlineNow = after.channel.members

            if before.channel != None:
                onlineBefore = before.channel.members

                if onlineBefore == onlineNow:
                    joined = False

        if after.channel == None:
            joined = False

        if joined:

            # If its a chimba, play the audio
            if member.name in gataChimba:

                voice_client = await after.channel.connect()
                voice_client.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source="./media/gataChimba/4.mp3"))
                await asyncio.sleep(5)
                await voice_client.disconnect()

            # Otherwise we see if the 4 bobos assembled to play a different audio
            elif not assembleTimeout:

                assembled = True
                online = []

                assembly_num = 0
                for user in after.channel.members:

                    if user.name not in bots:
                        if user.name not in bobos:
                            assembled = False
                        else:
                            assembly_num += 1

                if assembled and member.name in bobos and assembly_num == len(bobos):
                    voice_client = await after.channel.connect()
                    voice_client.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source="./media/avengers.mp3"))

                    assembleTimeout = True
                    await asyncio.sleep(40)
                    await voice_client.disconnect()
                    # disable for 6 hours
                    await asyncio.sleep(21600)

                    assembleTimeout = False


@client.event
async def on_command_error(ctx, error):

    error = str(error).lstrip("Command").rstrip("is not found")
    await ctx.send(
        "El komando" + error + " no exizte. No stas biendo kestoi chikita bieja tonta"
    )


@client.event
async def on_message(message):

    if (
        ";;" in message.content or "$botify" in message.content
    ) and "musicat" not in message.channel.name:
        await message.channel.send("Para poner temazos porfa vete al canal musicat")

    await client.process_commands(message)


@tasks.loop(hours=1)
async def birthday():
    birthday_gatas = tools.check_birthdays()
    # tools.check_birthdays() returns a list with the names of birthdayguys
    # example: ["Kurtony", "Mornet"]
    # check if it's 9am - time to wish a HB
    hour = datetime.datetime.now().hour
    if hour == 9 and len(birthday_gatas) > 0:
        # TO-DO (MORNET)
        general = tools.get_channel(client, 'general')

        await general.send(messages.MESSAGES[0][0] + ",".join(birthday_gatas) + messages.MESSAGES[0][1])


client.run("NzY2NDY5NTQ5NDczMjY3NzM0.X4j0ZA.GyWWwM1yQA5ni5jq-_lLJ1f1KtM")
